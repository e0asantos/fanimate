/**
* Fan_users.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models

email
Nombre
apellido
Username
password
(gmail) para vincular a gdrive

*/

module.exports = {
	connection: 'mysql',
  	attributes: {
    email:{
        required:true,
        minLength: 2,
        type:"email",
        unique: true
    },
    username:{
        type:"string",
        required:true,
        minLength: 2
    },
  	name:{
  			type:"string",
  			required:true,
		    minLength: 2
  		},
  		empnum:{
  			type:"string",
  		},
  		gtoken:{
  			type:"string",
  			required:false
  		},
  		gsecrettoken:{
  			type:"string",
  			required:false
  		},
  		hashseed:{
  			type:"string",
  			required:true
  		},
      rootFolder:{
        type:"string",
        required:false
      },
  		lastname:{
        type:"string",
  			required:true
  		},
      expiration_token:{
        type:"string",
        required:false
      },
      refresh_token:{
        type:"string",
        required:false
      }
  }
};

