/**
* Fan_tasks.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models

tabla 4- tareas (id,nombre,type,descripcion,tiempo,precio,status,worker)

*/

module.exports = {

  attributes: {
      author:{
        type:"string",
        required:false
      },
  		name:{
  			type:"string",
  			required:false
  		},
  		task_type:{
  			type:"string",
  			required:false
  		},
  		description:{
  			type:"string",
  			required:false
  		},
  		due_time:{
  			type:"string",
  			required:false
  		},
  		price:{
  			type:"string",
  			required:false
  		},
  		status:{
  			type:"string",
  			required:false
  		},
  		assigned_to:{
  			type:"string",
  			required:false
  		},
      folder_uid:{
        type:"string",
        required:false
      }

  }
};

