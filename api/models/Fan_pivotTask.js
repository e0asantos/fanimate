/**
* Fan_tasks.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models

tabla 4- tareas (id,nombre,type,descripcion,tiempo,precio,status,worker)

*/

module.exports = {

  attributes: {
  		projectid:{
  			type:"string",
  			required:false
  		},
      taskid:{
        type:"string",
        required:false
      },
      uid_folder:{
        type:"string",
        required:false
      },
      category:{
        type:"string",
        required:false
      },
      section:{
        type:"string",
        required:false
      }
  }
};

