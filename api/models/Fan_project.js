/**
* Fan_project.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models

tabla 1 -proyecto (id,imagen,nombre,productor,descripcion,licencia,fans_number)

*/

module.exports = {

  attributes: {
  		image_icon:{
  			type:"string",
  			required:false
  		},
  		name:{
  			type:"string",
  			required:false
  		},
  		producer:{
  			type:"string",
  			required:false
  		},
  		description:{
  			type:"string",
  			required:false
  		},
  		licenseType:{
  			type:"string",
  			required:false
  		},
      rootFolder:{
        type:"string",
        required:false
      },
  		fans_number:{
  			type:"string",
  			required:false
  		},
      buy_folder:{
        type:"string",
        required:false
      },
      review_folder:{
        type:"string",
        required:false
      },
      done_folder:{
        type:"string",
        required:false
      },
      task_folder:{
        type:"string",
        required:false
      },
      reference_folder:{
        type:"string",
        required:false
      },
  }
};

