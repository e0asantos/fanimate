/**
* Fan_files.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models


tabla 3- archivos (id,nombre,descripcion,proyecto,tarea)


*/

module.exports = {

  attributes: {
  		name:{
  			type:"string",
  			required:false
  		},
  		description:{
  			type:"string",
  			required:false
  		},
  		project:{
  			type:"string",
  			required:false
  		},
  		task:{
  			type:"string",
  			required:false
  		},
  }
};

