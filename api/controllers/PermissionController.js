
var readline = require('readline');
var google = require('googleapis');
var googleAuth = require('google-auth-library');
var SCOPES = ['https://www.googleapis.com/auth/drive'];
var rootScope
var rootScopeRed
var rootThis;
var rootAuth;
//var redirectUrlGlobal="http://14fdbc65.ngrok.com/callbackOauth"
var redirectUrlGlobal="http://fanimate.net/callbackOauth"

module.exports = {
  // Country.update({id: req.param('country_id'), image: {$exists: false}}, {image: newValue}, callback)
  registerProcess:function(req,res){
    var fullRequest=req.query;
    console.log(fullRequest);
    var usr=fullRequest.username;
    var pwd=fullRequest.password;
    Fan_users.find({name:usr,hashseed:pwd}).exec(function(err,itemsRelated){

      if (itemsRelated.length==0) {
        Fan_users.create({name:fullRequest.pname,lastname:fullRequest.lastname,hashseed:fullRequest.password,username:fullRequest.uname,email:fullRequest.email}).exec(function createdRecord(err,record){
          console.log('Created user with name ');
          //res.send("SUCCESS_REGISTERED");
          console.log(err)
          if (err!=null) {
            console.log("already_registered");
            res.send("ALREADY_REGISTERED");    
          } else {
            res.send("SUCCESS_REGISTERED");
          }
        }); 
      } else {
        console.log("already_registered");
        res.send("ALREADY_REGISTERED");
      }
      
       
    });
    
  },
  hello:function(req,res){
    res.send("hello");
  },
  initLinkage:function(){
    this.authorize("394424306587-f8d1sj6f950pko7otfsbqncui5pmbb4o.apps.googleusercontent.com","PRKWAc0Xdvw5oId499usFXNz",this.listFiles)
  },
  storeToken:function(token) {
    console.log('Token stored to ' + JSON.stringify(token));
  }
  ,listFiles:function(auth) {
  var service = google.drive('v2');
  service.files.list({
    auth: auth,
    maxResults: 10,
  }, function(err, response) {
    if (err) {
      console.log('The API returned an error: ' + err);
      return;
    }
    var files = response.items;
    if (files.length == 0) {
      console.log('No files found.');
    } else {
      console.log('Files:');
      for (var i = 0; i < files.length; i++) {
        var file = files[i];
        console.log('%s (%s)', file.title, file.id);
      }
    }
  });
  console.log("______folder")
  
},createAnimateRootFolder:function(auth){
  var service = google.drive('v2');
  rootAuth=auth
  newFolder=service.files.insert({
    auth: auth,
    resource: {
        mimeType: 'application/vnd.google-apps.folder',
        title: 'Fanimate-'+rootScope.session.currentUserObject.username
    }}, function(err,response){
      console.log(err)
      console.log("FOLDER_COMPLETE"+response.id)
      //save the folder of the root
      rootScope.session.currentUserObject.rootFolder=response.id
      var service = google.drive('v2');

      var body = {
        'value': rootScope.session.currentUserObject.email,
        'type': "user",
        'role': "owner"
      };
      console.log("Shared with:")
      console.log(body)
      service.permissions.insert({
          fileId: response.id,
          auth:rootAuth,
        resource: body
        },function(err,respuesta){
          console.log("share_error:"+err)
          console.log("share:"+respuesta)
        })

      Fan_users.update({username:rootScope.session.currentUserObject.username,hashseed:rootScope.session.currentUserObject.hashseed},{rootFolder: response.id}).exec(function afterwards(err, updated){
        if (err) {
          // handle error here- e.g. `res.serverError(err);`
          console.log("error on update")
          return;
        }
        //rootScope.session.currentUserObject=updated[0]
        console.log('Updated user to have name ' + updated[0].name);
        Fan_users.find({username:rootScope.session.currentUserObject.username,hashseed:rootScope.session.currentUserObject.hashseed}).exec(function getmeagain(err,response){
          rootScope.session.currentUserObject=response[0]
        })
      });


    }
  )
},authorize:function(mclientId,mclientSecret, callback) {
    var clientSecret = mclientId;
    var clientId = mclientSecret;
    var redirectUrl = redirectUrlGlobal
    var auth = new googleAuth();

    rootScope.session.oauth2Client = new auth.OAuth2(clientId, clientSecret, redirectUrl);

    this.getNewToken(rootScope.session.oauth2Client, callback);
  },
  getNewToken:function(oauth2Client, callback) {
    var authUrl = rootScope.session.oauth2Client.generateAuthUrl({
      access_type: 'offline',
      scope: SCOPES
    });
    console.log('Authorize this app by visiting this url: ', authUrl);
    rootScopeRed.redirect(authUrl);
    console.log('Enter the code from that page here: '+authUrl)
  },
  loginProcess: function (req, res) {
  	var fullRequest=req.query;
  	var usr=fullRequest.username;
  	var pwd=fullRequest.password;
    //this.hello("hello world");
  
	Fan_users.find({username:usr,hashseed:pwd}).exec(function (err, itemsRelated) {
  		console.log(itemsRelated);
  		if (itemsRelated.length>0) {
  			
        req.session.currentUserObject=itemsRelated[0];
        req.session.currentUser=itemsRelated[0].name;
        console.log("user found! "+itemsRelated[0].name+" :: "+itemsRelated[0].id);
  			res.send("Hi there! "+itemsRelated[0].name);	
  		} else {
  			console.log("user not found!");
  			res.send("NOT_LOGIN");
  		}
	});
  },
  signOutProcess: function (req, res) {
    req.session.currentUser=undefined;
    return res.redirect("/");
  },
  initDrive:function(req,res){
    rootScopeRed=res;
    rootScope=req
    rootThis=this
    //verify if the user doesnt have drive account
    if (req.session.currentUserObject==null || req.session.currentUserObject==undefined) {
      return res.redirect("/");
    };
    Fan_users.find({username:req.session.currentUserObject.username,hashseed:req.session.currentUserObject.hashseed}).exec(function (err, itemsRelated) {
      console.log(itemsRelated);
      if (itemsRelated.length>0) {

        //if (itemsRelated[0].rootFolder==null || itemsRelated[0].rootFolder==""){
          console.log("no drive configured")
          //res.send("no drive configured");
          rootThis.authorize("OmAC3E102bGORkRHIOkYdHoI","14728686169-sdv1ers14fqm5efaj264c8h7quajfp44.apps.googleusercontent.com",this.listFiles)


      
      } else {
        console.log("user not found!");
        res.send("NOT_LOGIN");
      }
  });

    //
  },
  storeAuth:function(req,res){
    
    console.log("=====>")
    console.log(req.query.code)
    console.log("<=====")
    rootThis=this
    //var auth = new googleAuth();
    //var oauth2Client = new auth.OAuth2("OmAC3E102bGORkRHIOkYdHoI","14728686169-sdv1ers14fqm5efaj264c8h7quajfp44.apps.googleusercontent.com", "https://6af07099.ngrok.com/callbackOauth");
    rootScope.session.oauth2Client.getToken(req.query.code, function(err,token){
      console.log("---->")
      console.log(token)
      if (token["refresh_token"]!=null && token["refresh_token"]!=undefined && token["refresh_token"]!="") {
          Fan_users.update({username:rootScope.session.currentUserObject.username,hashseed:rootScope.session.currentUserObject.hashseed},{gtoken: token.access_token,expiration_token:token.expiry_date,refresh_token:token.refresh_token}).exec(function afterwards(err, updated){
          if (err) {
            // handle error here- e.g. `res.serverError(err);`
            console.log("error on update")
            return;
          }
          Fan_users.find({email:"fanimatestudio@gmail.com"}).exec(function (err, itemsRelated) {
            rootScope.session.oauth2Client.credentials={token_type: 'Bearer',access_token:itemsRelated[0].gtoken,expiry_date:itemsRelated[0].expiration_token,refresh_token:itemsRelated[0].refresh_token};
            if(rootScope.session.currentUserObject.rootFolder=="" || rootScope.session.currentUserObject.rootFolder==undefined){
              rootThis.createAnimateRootFolder(rootScope.session.oauth2Client)
              console.log("<----")  
            }
          })
          console.log('storeAuth method() ' + updated[0].name);
        });
      } else {
        Fan_users.update({username:rootScope.session.currentUserObject.username,hashseed:rootScope.session.currentUserObject.hashseed},{gtoken: token.access_token,expiration_token:token.expiry_date}).exec(function afterwards(err, updated){
        if (err) {
          // handle error here- e.g. `res.serverError(err);`
          console.log("error on update")
          return;
        }
        Fan_users.find({email:"fanimatestudio@gmail.com"}).exec(function (err, itemsRelated) {
        rootScope.session.oauth2Client.credentials={token_type: 'Bearer',access_token:itemsRelated[0].gtoken,expiry_date:itemsRelated[0].expiration_token,refresh_token:itemsRelated[0].refresh_token};
          if(rootScope.session.currentUserObject.rootFolder=="" || rootScope.session.currentUserObject.rootFolder==undefined){
            rootThis.createAnimateRootFolder(rootScope.session.oauth2Client)
            console.log("<----")  
          }
        })
        console.log('storeAuth() ' + updated[0].name);
      });
      }
      
      //rootScope.session.oauth2Client.credentials=token
      //rootThis.listFiles(rootScope.session.oauth2Client)
      
      
    })
    //res.send("ok")
    res.redirect("/")
  }
};