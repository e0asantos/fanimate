var readline = require('readline');
var google = require('googleapis');
var googleAuth = require('google-auth-library');
var SCOPES = ['https://www.googleapis.com/auth/drive'];
var rootScope;
var reqScope;
var resScope;
var rootScopeRed
var rootThis;
var rootAuth;
var rootAnimateFolder;
var rootProjectRecord;
//var redirectUrlGlobal="http://14fdbc65.ngrok.com/callbackOauth"
var redirectUrlGlobal="http://fanimate.net/callbackOauth"
var pivotPermission;
module.exports = {
getHome:function(req,res){
	Fan_project.find({limit:10}).exec(function findCB(err, found){
	    console.log('Found User with name ' + found.length)
	    res.send(found)
	});
},
saveProject:function(req,res){
	console.log("touched");
	console.log(req.query)
	reqScope=req
	resScope=res;
	rootThis=this
	Fan_project.create({name:req.query.projectname1,description:req.query.description1}).exec(function createdRecord(err,record){
      console.log('Created project with contents ');
      console.log(record)
      rootThis.authorize(record);
      //res.send("SERVER_TOUCHED");
      
      if (err!=null) {
        console.log("already_registered");
        //res.send("ALREADY_REGISTERED");    
      };
    }); 
},authorize:function(projectRecord) {
    
    //resScope.send("ok")
    rootProjectRecord=projectRecord
    Fan_users.find({username:reqScope.session.currentUserObject.username,hashseed:reqScope.session.currentUserObject.hashseed}).exec(function (err, itemsRelated) {
  		console.log(itemsRelated);
  		if (itemsRelated.length>0) {
  			
		    Fan_users.find({email:"fanimatestudio@gmail.com"}).exec(function (err, itemsRelated) {
		    	console.log("master:"+itemsRelated)
		    	var clientSecret = "OmAC3E102bGORkRHIOkYdHoI";
			    var clientId = "14728686169-sdv1ers14fqm5efaj264c8h7quajfp44.apps.googleusercontent.com";
			    var redirectUrl = redirectUrlGlobal;
			    var auth = new googleAuth();

			    var oauth= new auth.OAuth2(clientId, clientSecret, redirectUrl);

		    	console.log({token_type: 'Bearer',access_token:itemsRelated[0].gtoken,expiry_date:itemsRelated[0].expiration_token,refresh_token:itemsRelated[0].refresh_token})
			    oauth.credentials={token_type: 'Bearer',access_token:itemsRelated[0].gtoken,expiry_date:itemsRelated[0].expiration_token,refresh_token:itemsRelated[0].refresh_token};
			    rootAnimateFolder=itemsRelated[0].rootFolder
		        //
		        //reqScope.session.currentUser=itemsRelated[0].name;
		        console.log("user found! "+itemsRelated[0].name+" :: "+itemsRelated[0].id);
	  			rootAuth=oauth
	  			Fan_users.find({username:reqScope.session.currentUserObject.username,hashseed:reqScope.session.currentUserObject.hashseed}).exec(function getmeagain(err,response){
		          reqScope.session.currentUserObject=response[0]
		          rootThis.createAnimateFolders(rootAuth,rootProjectRecord)
		        })
		    });

		    
  		} else {
  			console.log("user not found!");
  			res.send("NOT_LOGIN");
  		}
	});
  },createAnimateFolders:function(auth,projectRecord){
  	console.log("___>")
  	console.log(projectRecord)
  	console.log(reqScope.session.currentUserObject.rootFolder)
  	console.log("<___")
  var service = google.drive('v2');
  rootAuth=auth
  var newFolder=service.files.insert({
    auth: auth,
    resource: {
        mimeType: 'application/vnd.google-apps.folder',
        parents: [{"id":reqScope.session.currentUserObject.rootFolder}],

        title: projectRecord.name
    }}, function(err,response){
      
      //save the folder of the root
      console.log(err)
      console.log("FOLDER_COMPLETE: "+response)
      Fan_project.update({id:projectRecord.id},{rootFolder: response.id}).exec(function afterwards(err, updated){
        if (err) {
          // handle error here- e.g. `res.serverError(err);`
          console.log("error on update")
          return;
        }
        var body = {
	    'value': reqScope.session.currentUserObject.email,
	    'type': "user",
	    'role': "owner"
	  };
	  console.log("sharing with:")
	  console.log(body)
       
		

        console.log('created root folder: '+response.id);
      });
      
      service.files.insert({
	    auth: auth,
	    resource: {
	        mimeType: 'application/vnd.google-apps.folder',
	        parents: [{"id":response.id}],
	        title: "Buy"
	    }}, function(err,responsesub){
		    Fan_project.update({id:projectRecord.id},{buy_folder: responsesub.id}).exec(function afterwards(err, updated){
		        if (err) {
		          // handle error here- e.g. `res.serverError(err);`
		          console.log("error on buy_folder")
		          return;
		        }
		        pivotPermission=responsesub.id

		        var service = google.drive('v2');
			    service.permissions.list({
			    	fileId: pivotPermission,
			    	auth:rootAuth
			    },function(err,respuesta){
			    	console.log("===>permissions")
			    	console.log(respuesta)
			    	console.log("<===permissions")
			    	for (var i = respuesta.items.length - 1; i >= 0; i--) {
			    		if(respuesta.items[i].emailAddress.indexOf("fanimatestudio")<0){
			    			var servicein = google.drive('v2');
			    			  servicein.permissions.delete({
					        	fileId: pivotPermission,
					        	auth:rootAuth,
					        	permissionId: respuesta.items[i].id
					        },function(err,respuesta){
					        	console.log("LESS ERR"+err)
					        	console.log("LESS===>"+respuesta)
					        });
			    		}
			    	};
			    });
		      

		        console.log('Updated buy_folder ');
		      });
		})

      service.files.insert({
	    auth: auth,
	    resource: {
	        mimeType: 'application/vnd.google-apps.folder',
	        parents: [{"id":response.id}],
	        title: "Review"
	    }}, function(err,responsesub){
		    Fan_project.update({id:projectRecord.id},{review_folder: responsesub.id}).exec(function afterwards(err, updated){
		        if (err) {
		          // handle error here- e.g. `res.serverError(err);`
		          console.log("error on review_folder")
		          return;
		        }
		        console.log('Updated review_folder ');
		      });
		})

      service.files.insert({
	    auth: auth,
	    resource: {
	        mimeType: 'application/vnd.google-apps.folder',
	        parents: [{"id":response.id}],
	        title: "Tasks"
	    }}, function(err,responsesub){
		    Fan_project.update({id:projectRecord.id},{task_folder: responsesub.id}).exec(function afterwards(err, updated){
		        if (err) {
		          // handle error here- e.g. `res.serverError(err);`
		          console.log("error on task_folder")
		          return;
		        }
		        console.log('Updated task_folder ');
		      });
		})

      service.files.insert({
	    auth: auth,
	    resource: {
	        mimeType: 'application/vnd.google-apps.folder',
	        parents: [{"id":response.id}],
	        title: "Reference"
	    }}, function(err,responsesub){
		    Fan_project.update({id:projectRecord.id},{reference_folder: responsesub.id}).exec(function afterwards(err, updated){
		        if (err) {
		          // handle error here- e.g. `res.serverError(err);`
		          console.log("error on reference_folder")
		          return;
		        }
		        console.log('Updated reference_folder ');
		      });
		})

      service.files.insert({
	    auth: auth,
	    resource: {
	        mimeType: 'application/vnd.google-apps.folder',
	        parents: [{"id":response.id}],
	        title: "Done"
	    }}, function(err,responsesub){
		    Fan_project.update({id:projectRecord.id},{done_folder: responsesub.id}).exec(function afterwards(err, updated){
		        if (err) {
		          // handle error here- e.g. `res.serverError(err);`
		          console.log("error on done_folder")
		          return;
		        }
		        console.log('Updated done_folder ');
		      });
		})

      



    }
  )
  resScope.send("ok")
}
}
