var readline = require('readline');
var google = require('googleapis');
var googleAuth = require('google-auth-library');
var SCOPES = ['https://www.googleapis.com/auth/drive'];
var rootScope;
var reqScope;
var resScope;
var rootScopeRed
var rootThis;
var rootAuth;
var rootAnimateFolder;
var rootTask;
var rootProject;
//var redirectUrlGlobal="http://1ffdb613.ngrok.com/callbackOauth"
var redirectUrlGlobal="http://fanimate.net/callbackOauth"
var pivotPermission;
module.exports = {
	getAuthorTasks:function(req,res){
		console.log(req.allParams())
		console.log("saving into project:"+req.param("editor"))
		if (req.param("editor")==undefined) {
			return res.send([])
		};
		Fan_tasks.find({projectid:req.param("editor")}).exec(function folderChecker(err,foundResults){
      	if (err) {
      		console.log("error found when pivottask")
      	};
      	console.log("foundResults:"+foundResults.length)
      	//
      	res.send(foundResults)
      })
	},
	categories:function(req,res){
		Fan_categories.find({limit:10}).exec(function findCB(err, found){
		    console.log('Found categories ' + found.length)
		    res.send(found)
		});
	},
getHome:function(req,res){
	Fan_project.find({limit:10}).exec(function findCB(err, found){
	    console.log('Found User with name ' + found.length)
	    res.send(found)
	});
},
saveTask:function(req,res){
	console.log("touched");
	console.log(req.query)
	reqScope=req
	resScope=res;
	rootThis=this
	Fan_tasks.create({author:reqScope.session.currentUserObject.id,name:req.query.name1,task_type:req.query.tasktype1,description:req.query.description1,assigned_to:req.query.project1}).exec(function createdRecord(err,record){
      console.log('Created project with contents ');
      console.log(record)
      rootTask=record
      //decidir si se crean las carpetas de esa categoria
      Fan_pivotTask.find({category:record.task_type,projectid:req.query.project1}).exec(function folderChecker(err,foundResults){
      	if (err) {
      		console.log("error found when pivottask")
      	};
      	console.log("foundResults:"+foundResults.length)
      	//
      	if (foundResults.length==0) {
      		rootThis.authorize(record,true)		
      	} else {
      		rootThis.authorize(record,false)
      	}
      })
      
      //res.send("SERVER_TOUCHED");
      
      if (err!=null) {
        console.log("already_registered");
        //res.send("ALREADY_REGISTERED");    
      };
    }); 
},authorize:function(projectRecord,createFromScratch) {
    
    //resScope.send("ok")
    console.log(reqScope.session.currentUserObject)
    Fan_users.find({username:reqScope.session.currentUserObject.username,hashseed:reqScope.session.currentUserObject.hashseed}).exec(function (err, itemsRelated) {
  		console.log(itemsRelated);
  		if (itemsRelated.length>0) {
  			
		    Fan_users.find({email:"fanimatestudio@gmail.com"}).exec(function (err, itemsRelated) {
		    	console.log("master:"+itemsRelated)
		    	var clientSecret = "OmAC3E102bGORkRHIOkYdHoI";
			    var clientId = "14728686169-sdv1ers14fqm5efaj264c8h7quajfp44.apps.googleusercontent.com";
			    var redirectUrl = redirectUrlGlobal;
			    var auth = new googleAuth();

			    var oauth= new auth.OAuth2(clientId, clientSecret, redirectUrl);

		    	console.log({token_type: 'Bearer',access_token:itemsRelated[0].gtoken,expiry_date:itemsRelated[0].expiration_token,refresh_token:itemsRelated[0].refresh_token})
			    oauth.credentials={token_type: 'Bearer',access_token:itemsRelated[0].gtoken,expiry_date:itemsRelated[0].expiration_token,refresh_token:itemsRelated[0].refresh_token};
			    rootAnimateFolder=itemsRelated[0].rootFolder
		        //reqScope.session.currentUserObject=itemsRelated[0];
		        //reqScope.session.currentUser=itemsRelated[0].name;
		        console.log("user found! "+itemsRelated[0].name+" :: "+itemsRelated[0].id);
		        if (createFromScratch==true) {
		        	rootThis.createCategoryFolder(oauth,projectRecord)	
		        } else {
		        	rootThis.insertSingletask(oauth,projectRecord)
		        }
	  			
		    });

		    
  		} else {
  			console.log("user not found!");
  			res.send("NOT_LOGIN");
  		}
	});
  },insertSingletask:function(auth,taskRecord){
  	var service = google.drive('v2');
  	rootAuth=auth
  	Fan_project.find({id:taskRecord.assigned_to}).exec(function projectFound(err,projectRecord){
  		console.log("creating single task:")
  		console.log(projectRecord)
  		rootProject=projectRecord[0]
  		var service = google.drive('v2')
  		Fan_pivotTask.find({section:"reference",projectid:rootProject.id,category:rootTask.task_type}).exec(function pivotting(err,donef){
    		console.log(err)
    		console.log(donef)
    		var service = google.drive('v2');
	    	service.files.insert({
		    auth: rootAuth,
		    resource: {
		        mimeType: 'application/vnd.google-apps.folder',
		        parents: [{"id":donef[0].uid_folder}],
		        title: rootTask.name
		    }}, function(err,response1){
		    	console.log("created folder inside task")
		    	console.log(err)
		    	console.log(response1)
		    	Fan_tasks.update({assigned_to:rootProject.id,id:rootTask.id},{folder_uid:response1.id}).exec(function pivotting(err,donef){
			    	console.log(err)
			    })
		    })
    	})
  	})
  },createCategoryFolder:function(auth,taskRecord){
  	var service = google.drive('v2');
  	rootAuth=auth
  	Fan_project.find({id:taskRecord.assigned_to}).exec(function projectFound(err,projectRecord){
  		console.log("creating category:")
  		console.log(projectRecord)
  		rootProject=projectRecord[0]
  		if(projectRecord.length>0){
  			Fan_categories.find({id:rootTask.task_type}).exec(function gotCategoryName(err,gotName){
  				if (err) {
  					console.log("error found on find Fan_categories")
  				}
  				var service = google.drive('v2');
  				service.files.insert({
			    auth: rootAuth,
			    resource: {
			        mimeType: 'application/vnd.google-apps.folder',
			        parents: [{"id":rootProject.buy_folder}],
			        title: gotName[0].name
			    }}, function(err,response){
			    	console.log("created folder inside")
			    	console.log(err)
			    	console.log(response)
			    	Fan_pivotTask.create({section:"buy",projectid:rootProject.id,taskid:rootTask.id,uid_folder:response.id,category:rootTask.task_type}).exec(function pivotting(err,donef){
			    		console.log(err)
			    	})
			    })

			    service.files.insert({
			    auth: rootAuth,
			    resource: {
			        mimeType: 'application/vnd.google-apps.folder',
			        parents: [{"id":rootProject.done_folder}],
			        title: gotName[0].name
			    }}, function(err,response){
			    	console.log("created folder inside")
			    	console.log(err)
			    	console.log(response)
			    	Fan_pivotTask.create({section:"done",projectid:rootProject.id,taskid:rootTask.id,uid_folder:response.id,category:rootTask.task_type}).exec(function pivotting(err,donef){
			    		console.log(err)
			    	})
			    })



			    service.files.insert({
			    auth: rootAuth,
			    resource: {
			        mimeType: 'application/vnd.google-apps.folder',
			        parents: [{"id":rootProject.review_folder}],
			        title: gotName[0].name
			    }}, function(err,response){
			    	console.log("created folder inside")
			    	console.log(err)
			    	console.log(response)
			    	Fan_pivotTask.create({section:"review",projectid:rootProject.id,taskid:rootTask.id,uid_folder:response.id,category:rootTask.task_type}).exec(function pivotting(err,donef){
			    		console.log(err)
			    	})
			    })

			    service.files.insert({
			    auth: rootAuth,
			    resource: {
			        mimeType: 'application/vnd.google-apps.folder',
			        parents: [{"id":rootProject.reference_folder}],
			        title: gotName[0].name
			    }}, function(err,response){
			    	console.log("created folder inside")
			    	console.log(err)
			    	console.log(response)
			    	var service = google.drive('v2');
			    	service.files.insert({
				    auth: rootAuth,
				    resource: {
				        mimeType: 'application/vnd.google-apps.folder',
				        parents: [{"id":response.id}],
				        title: rootTask.name
				    }}, function(err,response1){
				    	console.log("created folder inside task")
				    	console.log(err)
				    	console.log(response)
				    	Fan_tasks.update({assigned_to:rootProject.id,id:rootTask.id},{folder_uid:response1.id}).exec(function pivotting(err,donef){
					    	console.log(err)
					    })
				    })
			    	Fan_pivotTask.create({section:"reference",projectid:rootProject.id,taskid:rootTask.id,uid_folder:response.id,category:rootTask.task_type}).exec(function pivotting(err,donef){
			    		console.log(err)
			    	})
			    })


			    service.files.insert({
			    auth: rootAuth,
			    resource: {
			        mimeType: 'application/vnd.google-apps.folder',
			        parents: [{"id":rootProject.task_folder}],
			        title: gotName[0].name
			    }}, function(err,response){
			    	console.log("created folder inside")
			    	console.log(err)
			    	console.log(response)
			    	
			    	


			    	Fan_pivotTask.create({section:"task",projectid:rootProject.id,taskid:rootTask.id,uid_folder:response.id,category:rootTask.task_type}).exec(function pivotting(err,donef){
			    		console.log(err)
			    	})

			    })	





  			})
  		}
  		
  	})
  	
  },createAnimateFolders:function(auth,projectRecord){
  	console.log("___>")
  	console.log(projectRecord)
  	console.log(reqScope.session.currentUserObject.rootFolder)
  	console.log("<___")
  var service = google.drive('v2');
  rootAuth=auth
  var newFolder=service.files.insert({
    auth: auth,
    resource: {
        mimeType: 'application/vnd.google-apps.folder',
        parents: [{"id":reqScope.session.currentUserObject.rootFolder}],

        title: projectRecord.name
    }}, function(err,response){
      
      //save the folder of the root
      console.log(err)
      console.log("FOLDER_COMPLETE: "+response)
      Fan_project.update({id:projectRecord.id},{rootFolder: response.id}).exec(function afterwards(err, updated){
        if (err) {
          // handle error here- e.g. `res.serverError(err);`
          console.log("error on update")
          return;
        }
        var body = {
	    'value': reqScope.session.currentUserObject.email,
	    'type': "user",
	    'role': "owner"
	  };
	  console.log("sharing with:")
	  console.log(body)
       
		

        console.log('created root folder: '+response.id);
      });
      
      service.files.insert({
	    auth: auth,
	    resource: {
	        mimeType: 'application/vnd.google-apps.folder',
	        parents: [{"id":response.id}],
	        title: "Buy"
	    }}, function(err,responsesub){
		    Fan_project.update({id:projectRecord.id},{buy_folder: responsesub.id}).exec(function afterwards(err, updated){
		        if (err) {
		          // handle error here- e.g. `res.serverError(err);`
		          console.log("error on buy_folder")
		          return;
		        }
		        pivotPermission=responsesub.id

		        var service = google.drive('v2');
			    service.permissions.list({
			    	fileId: pivotPermission,
			    	auth:rootAuth
			    },function(err,respuesta){
			    	console.log("===>permissions")
			    	console.log(respuesta)
			    	console.log("<===permissions")
			    	for (var i = respuesta.items.length - 1; i >= 0; i--) {
			    		if(respuesta.items[i].emailAddress.indexOf("fanimatestudio")<0){
			    			var servicein = google.drive('v2');
			    			  servicein.permissions.delete({
					        	fileId: pivotPermission,
					        	auth:rootAuth,
					        	permissionId: respuesta.items[i].id
					        },function(err,respuesta){
					        	console.log("LESS ERR"+err)
					        	console.log("LESS===>"+respuesta)
					        });
			    		}
			    	};
			    });
		      

		        console.log('Updated buy_folder ');
		      });
		})

      service.files.insert({
	    auth: auth,
	    resource: {
	        mimeType: 'application/vnd.google-apps.folder',
	        parents: [{"id":response.id}],
	        title: "Review"
	    }}, function(err,responsesub){
		    Fan_project.update({id:projectRecord.id},{review_folder: responsesub.id}).exec(function afterwards(err, updated){
		        if (err) {
		          // handle error here- e.g. `res.serverError(err);`
		          console.log("error on review_folder")
		          return;
		        }
		        console.log('Updated review_folder ');
		      });
		})

      service.files.insert({
	    auth: auth,
	    resource: {
	        mimeType: 'application/vnd.google-apps.folder',
	        parents: [{"id":response.id}],
	        title: "Tasks"
	    }}, function(err,responsesub){
		    Fan_project.update({id:projectRecord.id},{task_folder: responsesub.id}).exec(function afterwards(err, updated){
		        if (err) {
		          // handle error here- e.g. `res.serverError(err);`
		          console.log("error on task_folder")
		          return;
		        }
		        console.log('Updated task_folder ');
		      });
		})

      service.files.insert({
	    auth: auth,
	    resource: {
	        mimeType: 'application/vnd.google-apps.folder',
	        parents: [{"id":response.id}],
	        title: "Reference"
	    }}, function(err,responsesub){
		    Fan_project.update({id:projectRecord.id},{reference_folder: responsesub.id}).exec(function afterwards(err, updated){
		        if (err) {
		          // handle error here- e.g. `res.serverError(err);`
		          console.log("error on reference_folder")
		          return;
		        }
		        console.log('Updated reference_folder ');
		      });
		})

      service.files.insert({
	    auth: auth,
	    resource: {
	        mimeType: 'application/vnd.google-apps.folder',
	        parents: [{"id":response.id}],
	        title: "Done"
	    }}, function(err,responsesub){
		    Fan_project.update({id:projectRecord.id},{done_folder: responsesub.id}).exec(function afterwards(err, updated){
		        if (err) {
		          // handle error here- e.g. `res.serverError(err);`
		          console.log("error on done_folder")
		          return;
		        }
		        console.log('Updated done_folder ');
		      });
		})
      



    }
  )
  resScope.send("ok")
}
}
