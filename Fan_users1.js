/**
* Fan_users.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models

email
Nombre
apellido
Username
password
(gmail) para vincular a gdrive

*/

module.exports = {
	connection: 'mysql',
  	attributes: {
  	name:{
  			type:"string",
  			required:true,
		    minLength: 2
  		},
  		empnum:{
  			type:"string",
  			required:true
  		},
  		gtoken:{
  			type:"string",
  			required:false
  		},
  		gsecrettoken:{
  			type:"string",
  			required:false
  		},
  		hashseed:{
  			type:"string",
  			required:true
  		},
  		lastname:{
  			type:"email",
  			required:"true",
  			unique: true
  		}
  }
};

